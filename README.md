# Learning GitLab

This GitLab-project provides some material for learning GitLab. It is intended for students attending Software Engineering lectures at  [DHBW Karlsruhe](https://www.karlsruhe.dhbw.de/en/general/about-dhbw-karlsruhe.html) (_Baden-Württemberg Corporate State University in Karlsruhe_).

They have to do a complete software project within the context of that lecture. GitLab is used as the basic platform for collaboration, project management as well as all software engineering activities within these projects. Many students will be new to GitLab, but almost all have some basic experience with GitHub (which is the assumption in all documents herein).

The material presented in this GitLab-project isn't a replacement for the GitLab documentation. It is rather a usage concept describing how GitLab might be used in the aforementioned software engineering projects. What does that mean? 

Well, complex platforms and tools like GitLab offer a lot of functions and elements. This often overwhelming functionality is rarely intended to be used in its entirety. Quite to the contrary, you will only utilize a well defined subset depending on the purpose you want to use such a tool for. This subset must be carefully chosen so that it really serves your needs and so that it has a clearly defined application purpose within your work context. This is essentially meant by the term "usage concept".

So let's get started in the [Wiki](https://gitlab.com/dhbw-se/infos/-/wikis/home) to see what is different in GitLab and how its functions and elements may be used within these student assignments.
